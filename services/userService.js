const { v4 } = require('uuid');
const AWS = require('aws-sdk');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const { dictionary } = require('../assets/dictionary');

const createToken = (id) => {
    console.log(process.env.SECRET)
    return jwt.sign({ id }, process.env.SECRET, { expiresIn: '3h' })
}

const loginService = async (email, password) => {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.get({
        TableName: 'Users',
        Key: {
            email
        }
    }).promise();
    const user = result.Item;
    console.log(user);

    if (!user) {
        throw Error(dictionary.PASSWORD_OR_USER_WRONG);
    }

    const match = await bcrypt.compare(password, user.password);
    console.log(match);

    if (!match) {
        throw Error(dictionary.PASSWORD_OR_USER_WRONG);
    }

    const token = createToken(user.email);
    return token;
}

const signupService = async (email, password) => {


    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);

    const createdAt = new Date().toISOString();
    const id = v4();

    const newUser = {
        email,
        password: hash,
        createdAt
    };

    console.log("usuario", newUser);
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    await dynamodb.put({
        TableName: 'Users',
        Item: newUser
    }).promise();


    return newUser;
}

module.exports = {
    signupService,
    loginService
}