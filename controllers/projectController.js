const { v4 } = require('uuid');
const AWS = require('aws-sdk');

const createProject = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    const { db_name, db_user, projectName, pass, server } = JSON.parse(event.body);

    const createAt = new Date().toISOString();
    const id = v4();

    const newProject = {
      id,
      db_name,
      db_user,
      projectName,
      pass,
      server,
      createAt
    }

    await dynamodb.put({
      TableName: 'Projects',
      Item: newProject
    }).promise();

    return {
      status: 200,
      body: JSON.stringify(newProject)
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }

}


const getProjects = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.scan({
      TableName: 'Projects'
    }).promise();
    const projects = result.Items;

    return {
      status: 200,
      body: {
        projects: projects
      }
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const getProject = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const result = await dynamodb.get({
      TableName: 'Projects',
      Key: {
        id
      }
    }).promise();
    const project = result.Item;

    return {
      status: 200,
      body: { project: project }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const updateProject = async (event) => {
  try {
    const dynamobd = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const { db_name, db_user, projectName, pass, server } = JSON.parse(event.body);
    const result = await dynamobd.update({
      TableName: 'Projects',
      UpdateExpression: 'set db_name = :db_name, db_user = :db_user, projectName = :projectName, pass = :pass, server = :server',
      ExpressionAttributeValues: {
        ':db_name': db_name,
        ':db_user': db_user,
        ':projectName': projectName,
        ':pass': pass,
        ':server': server
      },
      Key: { id },
      ReturnValues: 'ALL_NEW'
    }).promise();
    return {
      status: 200,
      body: JSON.stringify({
        message: `Proyecto actualizado correctamente ${JSON.stringify(result)}`
      })
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const deleteProject = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;

    const result = await dynamodb.delete({
      TableName: 'Projects',
      Key: {
        id
      }
    }).promise();

    return {
      status: 200,
      body: { message: `Proyecto eliminado exitosamente ${id}` }
    }

  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}
const handler = async (event) => {
  switch (event.requestContext.http.method) {
    case 'GET':
      return event.pathParameters ? getProject(event) : getProjects(event);
    case 'POST':
      return createProject(event);
    case 'PUT':
      return updateProject(event);
    case 'DELETE':
      return deleteProject(event);
    case 'OPTIONS':
      return {
        status: 200,
      };

    default:
      return {
        status: 405,
        body: { msg: 'Method Not Allowed' },
      };
  }
};

module.exports = {
  handler,
};