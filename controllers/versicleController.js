const { v4 } = require('uuid');
const AWS = require('aws-sdk');

const createVersicle = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    const { book, number, rvr1960, ntv, lbla } = JSON.parse(event.body);
    const id = v4();
    const versicle = {
      id,
      book,
      number,
      rvr1960,
      ntv,
      lbla
    }

    await dynamodb.put({
      TableName: 'Versicles',
      Item: versicle
    }).promise();

    return {
      status: 200,
      body: JSON.stringify(versicle)
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }
    };
  }

}


const getVersicles = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.scan({
      TableName: 'Versicles'
    }).promise();
    const versicles = result.Items;

    return {
      status: 200,
      body: {
        versicles
      }
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const getVersicle = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const result = await dynamodb.get({
      TableName: 'Versicles',
      Key: {
        id
      }
    }).promise();
    const versicle = result.Item;

    return {
      status: 200,
      body: { versicle }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const updateVersicle = async (event) => {
  try {
    const dynamobd = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const { book, number, rvr1960, ntv, lbla } = JSON.parse(event.body);
    const result = await dynamobd.update({
      TableName: 'Versicles',
      UpdateExpression: 'set book = :book, number = :number, rvr1960 = :rvr1960, ntv = :ntv, lbla = :lbla',
      ExpressionAttributeValues: {
        ':book': book,
        ':number': number,
        ':rvr1960': rvr1960,
        ':ntv': ntv,
        ':lbla': lbla
      },
      Key: { id },
      ReturnValues: 'ALL_NEW'
    }).promise();
    return {
      status: 200,
      body: JSON.stringify({
        message: `Versículo actualizado correctamente ${JSON.stringify(result)}`
      })
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const massiveBooks = async (event) => {
  AWS.config.update({ region: 'us-east-1' });
  const dynamodb = new AWS.DynamoDB.DocumentClient();

  const result = await dynamodb.scan({
    TableName: 'Books'
  }).promise();
  const books = result.Items;
  const dynamoBooksCodes = books.map(book => book.code);

  const axios = require('axios');
  const response1 = await axios.get(`https://biblia.my.to/book`);
  const booksAPI = response1.data
  console.log(booksAPI);
  let notImportedBooks = [];
  let importedBooks = [];

  booksAPI.map(async (book) => {
    if (!dynamoBooksCodes.includes(book.id)) {

      try {
        const chapters = await fetchChapters(book.id);
        const bookToInsert = {
          id: v4(),
          code: book.id,
          abbreviation: book.abbreviation,
          shortName: book.name,
          longName: book.nameLong,
          chapters,
        }

        await dynamodb.put({
          TableName: 'Books',
          Item: bookToInsert
        }).promise();

        importedBooks.push(book.name);
      } catch (error) {
        console.log(error);
        notImportedBooks.push(book.name);
      }
    }
  });
  console.log('Imported books', importedBooks);
  console.log('Not Imported Books', notImportedBooks);
}

const fetchChapters = async (bookCode) => {
  const axios = require('axios');
  const response = await axios.get(`https://biblia.my.to/book/${bookCode}/chapter`);

  const chapters = response.data.map((chapter) => {
    chapter.id = v4();
    return chapter;
  });
  console.log(chapters);
  return chapters;
}


function fetchVersesFromApi(bookCode, chapter) {
  var request = require('request');
  return new Promise((resolve, reject) => {
    request(`https://biblia.my.to/book/${bookCode}/chapter/${chapter}/verse`, function (error, response, body) {
      if (!error && response.statusCode === 200) {
        const verses = JSON.parse(body).map((chapter) => {
          chapter.id = v4();
          return chapter;
        });
        resolve(verses);
      } else {
        console.error('Error al obtener los versículos');
        reject(new Error('Error al obtener los versículos'));
      }
    });
  });
}

const massiveChapters = async (event) => {
  try {
    AWS.config.update({ region: 'us-east-1' });
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.scan({
      TableName: 'Books'
    }).promise();
    const books = result.Items;
    books.map(async (book) => {
      const chapters = await fetchChapters(book.code)
      console.log(chapters)

      await dynamodb.update({
        TableName: 'Books',
        UpdateExpression: 'set chapters = :chapters',
        ExpressionAttributeValues: {
          ':chapters': chapters
        },
        Key: { id: book.id },
        ReturnValues: 'ALL_NEW'
      }).promise();
    });
    return {
      status: 200,
      body: { msg: 'Success' }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const massiveVersicles = async () => {
  try {
    AWS.config.update({ region: 'us-east-1' });
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.scan({
      TableName: 'Books'
    }).promise();
    const books = result.Items;
    books.map(async (book) => {
      const chapters = book.chapters;
      chapters.map(async (chapter, i) => {
        const verses = await fetchVersesFromApi(book.code, chapter);
        chapter[i].verses = verses;

      })
      console.log(chapters)
      // await dynamodb.update({
      //   TableName: 'Books',
      //   UpdateExpression: 'set chapters = :chapters',
      //   ExpressionAttributeValues: {
      //     ':chapters': chapters
      //   },
      //   Key: { id: book.id },
      //   ReturnValues: 'ALL_NEW'
      // }).promise();
    });
    return {
      status: 200,
      body: { msg: 'Success' }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}
const deleteVersicle = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;

    const result = await dynamodb.delete({
      TableName: 'Versicles',
      Key: {
        id
      }
    }).promise();

    return {
      status: 200,
      body: { message: `Versículo eliminado exitosamente ${id}` }
    }

  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const handler = async (event) => {
  switch (event.requestContext.http.method) {
    case 'GET':
      return event.pathParameters ? getVersicle(event) : getVersicles(event);
    case 'POST':
      return !event.pathParameters ? massive(event) : createVersicle(event);
    case 'OPTIONS':
      return {
        status: 200,
      };
    case 'PUT':
      return updateVersicle(event);
    case 'DELETE':
      return deleteVersicle(event);
    default:
      return {
        status: 405,
        body: { msg: 'Method Not Allowed' },
      };
  }
};

massiveBooks();
// fetchChapters('DEU');

module.exports = {
  handler,
};

