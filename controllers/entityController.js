const { v4 } = require('uuid');
const AWS = require('aws-sdk');

const getEntities = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.scan({
      TableName: 'Entities'
    }).promise();
    const entities = result.Items;

    return {
      status: 200,
      body: {
        entities: entities
      }
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const createEntity = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { entityName, projectId } = JSON.parse(event.body);

    const createAt = new Date().toISOString();
    const id = v4();
    const newEntity = {
      id,
      entityName,
      projectId,
      createAt
    }

    await dynamodb.put({
      TableName: 'Entities',
      Item: newEntity
    }).promise();

    return {
      statusCode: 200,
      body: JSON.stringify(newEntity)
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }

}

const getEntity = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const result = await dynamodb.get({
      TableName: 'Entities',
      Key: {
        id
      }
    }).promise();
    const entity = result.Item;

    return {
      status: 200,
      body: { entity: entity }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const updateEntity = async (event) => {
  try {
    const dynamobd = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const { entityName, projectId } = JSON.parse(event.body);
    const result = await dynamobd.update({
      TableName: 'Entities',
      UpdateExpression: 'set entityName = :entityName, projectId = :projectId',
      ExpressionAttributeValues: {
        ':entityName': entityName,
        ':projectId': projectId,
      },
      Key: { id },
      ReturnValues: 'ALL_NEW'
    }).promise();
    return {
      status: 200,
      body: JSON.stringify({
        msg: `Entidad actualizada correctamente ${JSON.stringify(result)}`
      })
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const deleteEntity = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;

    await dynamodb.delete({
      TableName: 'Entities',
      Key: {
        id
      }
    }).promise();

    return {
      status: 200,
      body: { msg: `Entidad eliminada exitosamente ${id}` }
    }

  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const handler = async (event) => {
  switch (event.requestContext.http.method) {
    case 'GET':
      return event.pathParameters ? getEntity(event) : getEntities(event);
    case 'POST':
      return createEntity(event);
    case 'PUT':
      return updateEntity(event);
    case 'DELETE':
      return deleteEntity(event);
    case 'OPTIONS':
      return {
        status: 200,
      };
    default:
      return {
        status: 405,
        body: { msg: 'Method Not Allowed' },
      };
  }
};

module.exports = {
  handler,
};