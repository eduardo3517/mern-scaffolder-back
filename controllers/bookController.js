const { v4 } = require('uuid');
const AWS = require('aws-sdk');

const createBook = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();

    const { abbreviation, chapters, code, longName, shortName, classification, ordering } = JSON.parse(event.body);
    const id = v4();
    const book = {
      id,
      abbreviation,
      chapters,
      code,
      longName,
      shortName,
      classification,
      ordering
    }

    await dynamodb.put({
      TableName: 'Books',
      Item: book
    }).promise();

    return {
      status: 200,
      body: JSON.stringify(book)
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }
    };
  }

}

const getBooks = async (event) => {
  try {
    let responseBook = [];
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    let lek;
    do {
      result = await dynamodb.scan({
        ExclusiveStartKey: lek,
        TableName: 'Books'
      }).promise();

      const books = result.Items;
      responseBook = responseBook.concat(books);
      lek = result.LastEvaluatedKey
    } while (lek != undefined);

    return {
      status: 200,
      body: {
        responseBook
      }
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const getBook = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const result = await dynamodb.get({
      TableName: 'Books',
      Key: {
        id
      }
    }).promise();
    const book = result.Item;

    return {
      status: 200,
      body: { book }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const updateBook = async (event) => {
  try {
    const dynamobd = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const { abbreviation,
      chapters,
      code,
      longName,
      shortName,
      classification,
      ordering
    } = JSON.parse(event.body);
    const result = await dynamobd.update({
      TableName: 'Books',
      UpdateExpression: 'set abbreviation = :abbreviation, chapters = :chapters, code = :code, longName = :longName, shortName = :shortName, classification = :classification, ordering = :ordering',
      ExpressionAttributeValues: {
        ':abbreviation': abbreviation,
        ':chapters': chapters,
        ':code': code,
        ':longName': longName,
        ':shortName': shortName,
        ':classification': classification,
        ':ordering': ordering
      },
      Key: { id },
      ReturnValues: 'ALL_NEW'
    }).promise();
    return {
      status: 200,
      body: JSON.stringify({
        message: `Libro actualizado correctamente ${JSON.stringify(result)}`
      })
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const fetchVersesFromApi = async (bookCode, chapter) => {
  console.log("params", bookCode, chapter)
  const axios = require('axios');

  try {
    const response1 = await axios.get(`https://biblia.my.to/book/${bookCode}/chapter/${chapter}/verse`);
    const versesAmount = response1.data.length;

    const response2 = await axios.get(`https://biblia.my.to/book/${bookCode}/chapter/${chapter}/verse/1-${versesAmount}`);
    const verses = response2.data.map((verse) => {
      verse.id = v4();
      return verse;
    });
    console.log(verses);
    return verses;
  } catch (error) {
    console.error('Error en la solicitud:', error);
    throw new Error('Error en el get');
  }
};


const importVerses = async (event) => {
  const { bookId, chapterNumber } = JSON.parse(event.body);
  const dynamodb = new AWS.DynamoDB.DocumentClient();
  try {
    const book = await dynamodb.get({
      TableName: 'Books',
      Key: {
        id: bookId
      }
    }).promise();
    const foundBook = book.Item;
    const { chapters, code } = foundBook;
    const verses = await fetchVersesFromApi(code, chapterNumber);

    chapters.forEach((chapter) => {
      if (chapter.number === chapterNumber) {
        chapter.verses = verses;
      }
    });
    console.log("chapts", foundBook);

    await dynamodb.update({
      TableName: 'Books',
      UpdateExpression: 'set chapters = :chapters',
      ExpressionAttributeValues: {
        ':chapters': chapters
      },
      Key: { id: bookId },
      ReturnValues: 'ALL_NEW'
    }).promise();
    return {
      status: 200,
      body: { verses }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const deleteBook = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;

    await dynamodb.delete({
      TableName: 'Books',
      Key: {
        id
      }
    }).promise();

    return {
      status: 200,
      body: { message: `Libro eliminado exitosamente ${id}` }
    }

  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const handler = async (event) => {
  switch (event.requestContext.http.method) {
    case 'GET':
      return event.pathParameters ? getBook(event) : getBooks(event);
    case 'POST':
      return JSON.parse(event.body).chapterNumber ? importVerses(event) : createBook(event);
    case 'OPTIONS':
      return {
        status: 200,
      };
    case 'PUT':
      return updateBook(event);
    case 'DELETE':
      return deleteBook(event);
    default:
      return {
        status: 405,
        body: { msg: 'Method Not Allowed' },
      };
  }
};

module.exports = {
  handler,
};

