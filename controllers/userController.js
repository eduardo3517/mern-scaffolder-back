
const AWS = require('aws-sdk');
const { signupService, loginService } = require('../services/userService');
const validator = require('validator');

const signup = async (event) => {

  const { email, password } = JSON.parse(event.body);



  if (!email || !password) {
    return {
      status: 400,
      body: { msg: `Hay campos vacíos o no se reconocen.` }
    }
  }
  if (!validator.isEmail(email)) {
    return {
      status: 400,
      body: { msg: `El valor no corresponde a un correo electrónico.` }
    }
  }
  if (!validator.isStrongPassword(password)) {
    return {
      status: 400,
      body: {
        msg: `La constraseña es débil. Por favor ingrese una contraseña que contenga mayúsculas, minúsculas, números y caracteres especiales.`
      }
    }
  }
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.get({
      TableName: 'Users',
      Key: {
        email
      }
    }).promise();
    const user = result.Item;

    if (user) {
      return {
        status: 500,
        body: { msg: `El usuario ya se encuentra registrado` }
      }
    }
    const newUser = await signupService(email, password);
    return {
      status: 200,
      body: JSON.stringify(newUser)
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }

}


const getUsers = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const result = await dynamodb.scan({
      TableName: 'Users'
    }).promise();
    const users = result.Items;

    return {
      status: 200,
      body: {
        projects: users
      }
    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const login = async (event) => {

  const { email, password } = JSON.parse(event.body);

  if (!email || !password) {
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
  try {
    const token = await loginService(email, password);

    return {
      status: 200,
      body: { token }

    };
  } catch (error) {
    console.log("log erroring", error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const getUser = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const result = await dynamodb.get({
      TableName: 'Users',
      Key: {
        email
      }
    }).promise();
    const user = result.Item;

    return {
      status: 200,
      body: { user: user }

    };
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const updateUser = async (event) => {
  try {
    const dynamobd = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;
    const { email, password } = JSON.parse(event.body);
    const result = await dynamobd.update({
      TableName: 'Users',
      UpdateExpression: 'set email = :email, password = :password',
      ExpressionAttributeValues: {
        ':email': email,
        ':password': password,
      },
      Key: { id },
      ReturnValues: 'ALL_NEW'
    }).promise();
    return {
      status: 200,
      body: {
        msg: `Usuario actualizado correctamente ${JSON.stringify(result)}`
      }
    }
  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}

const deleteUser = async (event) => {
  try {
    const dynamodb = new AWS.DynamoDB.DocumentClient();
    const { id } = event.pathParameters;

    await dynamodb.delete({
      TableName: 'Users',
      Key: {
        id
      }
    }).promise();

    return {
      status: 200,
      body: { msg: `Usuario eliminado exitosamente ${id}` }
    }

  } catch (error) {
    console.log(error);
    return {
      status: 500,
      body: { msg: error.message }

    };
  }
}
const handler = async (event) => {
  console.log(JSON.stringify(event))
  switch (event.requestContext.http.method) {
    case 'GET':
      return getUsers(event);
    case 'POST':
      return event.rawPath === '/users/signup' ? signup(event) : login(event);
    case 'PUT':
      return updateUser(event);
    case 'DELETE':
      return deleteUser(event);
    case 'OPTIONS':
      return {
        status: 200,
      };

    default:
      return {
        status: 405,
        body: { msg: 'Method Not Allowed' },
      };
  }
};

module.exports = {
  handler,
};